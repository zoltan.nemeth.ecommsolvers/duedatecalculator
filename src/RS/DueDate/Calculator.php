<?php
/**
 * @author   Németh Zoltán <signred@gmail.com>
 * @licence  GNU Public Licence <https://www.gnu.org/licenses/gpl-3.0.en.html>
 * @created  2017. 12. 10. 11:18
 */

namespace RS\DueDate;

/** Class RS\DueDate\Calculator */
class Calculator {
    
    const MONDAY    = 1;
    const TUESDAY   = 2;
    const WEDNESDAY = 3;
    const THURSDAY  = 4;
    const FRIDAY    = 5;
    const SATURDAY  = 6;
    const SUNDAY    = 7;
    
    const DAILY_WORK_HOURS = 8;
    const DAILY_WORK_CLOSE_HOUR = 17;
    const DAILY_WORK_OPEN_HOUR = 9;
    
    const FORMAT_WEEKDAY = 'N';
    const FORMAT_FULL = 'Y-m-d H:i';
    
    /** @var  \DateTime $startDate */
    private $startDate;
    /** @var  \DateTime $date */
    private $date;
    
    /** @var \int[] $weekdays */
    private $freeWeekDays = [];
    
    private $tarHours = 0;
    
    /**
     * Sets start date
     *
     * @param \DateTime $date Start date with time
     *
     * @return $this
     */
    public function setDate(\DateTime $date) {
        $this->date = clone $date;
        $this->startDate = $date;
        
        return $this;
    }
    
    public function getStartDate() {
        return $this->startDate;
    }
    
    /**
     * Get start date (if null, then set to now)
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }
    
    /**
     * Sets free week days
     *
     * @param array $freeDays Non repeated values
     *
     * @return $this
     */
    public function setFreeDays(array $freeDays) {
        $this->freeWeekDays = $freeDays;
        return $this;
    }
    
    /**
     * Get free week days
     *
     * @return int[]
     */
    public function getFreeDays() {
        return $this->freeWeekDays;
    }
    
    /**
     * Calculate working days
     *
     * @return $this
     */
    public function addWorkDays()
    {
        $workDays = $this->getTurnARoundDays();
        $count = 0;
        while ($count < $workDays) {
            $this->getDate()->modify('+1 day');
            if ($this->isWorkDay()) {
                $count++;
            }
        }
        
        return $this;
    }
    
    /**
     * Check working day
     *
     * @return bool
     */
    public function isWorkDay() {
        if (!$this->isFreeWeekDay()) {
            return true;
        }
        return false;
    }
    
    /**
     * Is free day on the week
     *
     * @return bool
     */
    public function isFreeWeekDay() {
        $currentDay = (int)$this->date->format(self::FORMAT_WEEKDAY);
        if (in_array($currentDay, $this->getFreeDays())) {
            return true;
        }
        return false;
    }
    
    /**
     * Set turn a round hours
     *
     * @param $hours
     *
     * @return $this
     */
    public function setTurnARound($hours) {
        $this->tarHours = (int)$hours;
        
        return $this;
    }
    
    /**
     * Get turn a round hours
     *
     * @return int
     */
    public function getTurnARound() {
        return $this->tarHours;
    }
    
    /**
     * Get turn a round days
     *
     * @return int
     */
    public function getTurnARoundDays()
    {
        return (int)($this->tarHours / self::DAILY_WORK_HOURS);
    }
    
    /**
     * getTurnARoundHours
     *
     * @return int
     */
    public function getTurnARoundHours()
    {
        return (int)($this->tarHours % self::DAILY_WORK_HOURS);
    }
    
    /**
     * Add working hours
     */
    public function addWorkHours() {
        $workHours = $this->getTurnARoundHours();
        if ($workHours === 0) {
            return;
        }
        $count = 0;
        while ($count < $workHours) {
            $currentHour = (int)$this->getDate()->add(new \DateInterval('PT1H'))->format('H');
            if ($currentHour >= self::DAILY_WORK_CLOSE_HOUR) {
                $this->getDate()->modify('+1 day');
                while (in_array($this->getDate()->format(self::FORMAT_WEEKDAY), $this->getFreeDays())) {
                    $this->getDate()->modify('+1 day');
                }
                $this->getDate()->setTime(self::DAILY_WORK_OPEN_HOUR, $this->getDate()->format('i'));
            }
            $count++;
        }
    }
    
    /**
     * Due date calculation
     */
    public function calculate() {
        $this->addWorkDays();
        $this->addWorkHours();
        
        return $this;
    }
    
}